angular.module('teamtweets.services', ['http-auth-interceptor', 'restangular'])

  .factory('RESTService', function (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl('http://192.168.1.67:8000/api/v1/');
      // Django expects trailing slashes
      RestangularConfigurer.setRequestSuffix('/');
      // DRF returns data nested with meta data
      RestangularConfigurer.addResponseInterceptor(function (data) {
        var extractedData = data.results;
        extractedData.meta = {
          count: data.count,
          next: data.next,
          previous: data.previous
        };
        return extractedData;
      });
    });
  })

  .factory('Teams', function (RESTService) {
    return RESTService.service('teams');
  })


  .factory('RegistrationService', function ($http, authService) {
    return {
      register: function (user) {
        $http.post('http://192.168.1.67:8000/api/v1/profile/register/',
          { username: user.username, password: user.password }, { ignoreAuthModule: true })
          .success(function (data, status, headers, config) {
            $http.defaults.headers.common.Authorization = 'Token ' + data.token;

            authService.loginConfirmed(data, function (config) {
              config.headers.Authorization = 'Token ' + data.token;
              return config;
            });
          })
          .error(function (data, status, headers, config) {
            // TODO - sign up failed
            $rootScope.$broadcast('event:auth-login-failed', status);
          });
      }
    }
  })

  .factory('AuthenticationService', function ($rootScope, $http, authService) {
    return {
      login: function (user) {
        // see: http://www.kdmooreconsulting.com/blogs/authentication-with-ionic-and-angular-js-in-a-cordovaphonegap-mobile-web-application/
        $http.post('http://192.168.1.67:8000/api/v1/profile/token-auth/',
          { username: user.username, password: user.password }, { ignoreAuthModule: true })
          .success(function (data, status, headers, config) {
            $http.defaults.headers.common.Authorization = 'Token ' + data.token;

            // Need to inform the http-auth-interceptor that
            // the user has logged in successfully.  To do this, we pass in a function that
            // will configure the request headers with the authorization token so
            // previously failed requests(aka with status == 401) will be resent with the
            // authorization token placed in the header
            authService.loginConfirmed(data, function (config) {
              config.headers.Authorization = 'Token ' + data.token;
              return config;
            });
          })
          .error(function (data, status, headers, config) {
            $rootScope.$broadcast('event:auth-login-failed', status);
          });
      },
      logout: function (user) {
        delete $http.defaults.headers.common.Authorization;
        $rootScope.$broadcast('event:auth-logout-complete');
      },
      loginCancelled: function () {
        authService.loginCancelled();
      }
    };
  })
;