angular.module('teamtweets', ['ionic', 'teamtweets.controllers', 'teamtweets.services'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html"
      })

      .state('app.teams', {
        url: "/teams",
        views: {
          'menuContent': {
            templateUrl: "templates/teams.html",
            controller: 'TeamCtrl'
          }
        }
      })

      .state('app.players', {
        url: "/players",
        views: {
          'menuContent': {
            templateUrl: "templates/players.html"
          }
        }
      })

      .state('app.settings', {
        url: "/settings",
        views: {
          'menuContent': {
            templateUrl: "templates/settings.html"
          }
        }
      })

      .state('login', {
        url: "/login",
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      })

      .state('logout', {
        url: "/logout",
        controller: 'LogoutCtrl'
      })

      .state('register', {
        url: "/register",
        templateUrl: "templates/register.html",
        controller: 'RegistrationCtrl'
      })
    ;
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
  });

