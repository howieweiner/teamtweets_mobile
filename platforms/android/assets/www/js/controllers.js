angular.module('teamtweets.controllers', ['teamtweets.services'])

  .controller('AppCtrl', function ($rootScope, $scope, $state) {
    $scope.loggedIn = true;

    $rootScope.$on('$stateChangeStart', function (event, toState) {
      if ((toState.name !== 'login') && (!$scope.loggedIn)) {
        event.preventDefault();
        $state.go('login');
      }
    });
  })

  .controller('LoginCtrl', function ($rootScope, $scope, $state, AuthenticationService) {
    $scope.user = {
      username: null,
      password: null
    };

    $scope.login = function (user) {
      AuthenticationService.login($scope.user);
    };

    $rootScope.$on('event:auth-loginRequired', function (e, rejection) {
      $scope.loggedIn = false;
      $state.go('login');
    });

    $rootScope.$on('event:auth-loginConfirmed', function () {
      $scope.loggedIn = true;
      $state.go('app.teams');
    });

    $rootScope.$on('event:auth-login-failed', function (e, status) {
      var error = "Login failed.";
      if (status == 401) {
        error = "Invalid Username or Password.";
      }
      // TODO
      $scope.message = error;
    });

    $rootScope.$on('event:auth-logout-complete', function () {
      $scope.loggedIn = false;
      $state.go('login');
    });
  })

  .controller('LogoutCtrl', function ($scope, AuthenticationService) {
    AuthenticationService.logout();
  })

  .controller('RegistrationCtrl', function ($scope, RegistrationService) {
    $scope.register = function(user) {
      RegistrationService.register(user);
    };
  })

  .controller('TeamCtrl', function ($scope, $state, Teams) {
    Teams.getList().then(function (teams) {
      $scope.teams = teams;
    });
  })

;
